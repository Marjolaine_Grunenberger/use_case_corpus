from config.config_dow_jones import avro_field_names

def get_date_article(article):
    return article[avro_field_names["date"]] / 1000