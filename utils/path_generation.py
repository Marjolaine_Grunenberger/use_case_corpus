import os
from config.path import path_preprocess, path_input


def get_preprocess_path(filename):
    return os.path.join(path_preprocess, filename)

def get_input_path(filename):
    return os.path.join(path_input, filename)