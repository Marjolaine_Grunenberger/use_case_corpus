from utils.utils_for_string import to_standard
from utils.path_generation import get_input_path
from config.config_dow_jones import avro_field_names
import os
import datetime
import re


class Corpus:
    def __init__(self, dict_articles):
        self.dict_articles = dict_articles


    def filter_by_topic(self, list_to_exclude, list_of_words):
        #list_of_words_to_exclude = (r"tomb[a-z]{1,5} à l.eau", "fil de l.eau")
        #list_of_words_topic = ("éoliennes", "travaux", "environnement", "grand projet", "", "phréatique")
        cleaned_dict_articles = {}
        for id in self.dict_articles:
            article = self.dict_articles[id]
            for to_exclude in list_to_exclude:
                new_titre = re.sub(to_exclude.lower(), '', (article[avro_field_names['titre']].lower()))
                for word in list_of_words:
                    if to_standard(word) in to_standard(new_titre):
                        cleaned_dict_articles[id] = self.dict_articles[id]
        self.dict_articles = cleaned_dict_articles


    def filter_by_date(self, date_min, date_max):
        self.dict_articles = {c: self.dict_articles[c] for c in self.dict_articles if date_min <= datetime.datetime.utcfromtimestamp(
            self.dict_articles[c][avro_field_names["date"]] / 1000).date() and date_max >= datetime.datetime.utcfromtimestamp(
            self.dict_articles[c][avro_field_names["date"]] / 1000).date()}


    def remove_sport(self, remove_sport=True):
        if remove_sport:
            self.dict_articles = {c: self.dict_articles[c] for c in self.dict_articles if 'gspo' not in self.dict_articles[c][avro_field_names["sujet"]]}


    def filter_by_location(self):
        cleaned_dict_articles = {}
        loc = open(get_input_path('list_locations_of_interest.txt'), 'r')
        list_loc_of_interest = [line.rstrip(os.linesep) for line in loc]
        set_loc_of_interest_norm = set([to_standard(loc) for loc in list_loc_of_interest])
        for id in self.dict_articles:
            try:
                set_loc_article_norm = set(
                    [to_standard(loc) for loc in self.dict_articles[id]['loc']])
                if len(set_loc_article_norm.intersection(set_loc_of_interest_norm)) > 0:
                    cleaned_dict_articles[id] = self.dict_articles[id]
            except KeyError:
                print('Erreur de clé for article %s mais ok' % id)
        self.dict_articles = cleaned_dict_articles


    def process_corpus(self, remove_sport=True, filter_by_location=False, filter_by_topic=False, filter_by_date=False, title=False):
        params = locals()
        for p in params:
            if params[p] and p != 'self':
                self.p
